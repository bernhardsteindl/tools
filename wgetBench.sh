#!/bin/sh
## wgetBench 1.1
## uses speedtest.tele2.net for penetrating your network
## GPLv3, part of e9aTools; https://gitlab.com/hniestl/tools
##
command="wget -bq -O /dev/null "
url="http://speedtest.tele2.net/"$2".zip"
##
##
echo "#########################"
echo "#### wgetBench v1.1 #####"
echo "#########################"
echo "Part of e9aTools; https://gitlab.com/hniestl/tools"
##
## Check for params
##
if [ -z $1 ]
then
  echo "ERROR. number of streams not set"
  echo "Usage: ./wgetBench <NumberOfStreams> <Size>"
  echo "Example: ./wgetBench 1000 100MB"
  exit 2
fi
if [ -z $2 ]
then
  echo "ERROR. size not set"
  echo "Usage: ./wgetBench <NumberOfStreams> <Size>"
  echo "Example: ./wgetBench 1000 100MB"
  echo "Available sizes:
1000GB
100GB
100kB
100MB
10GB
10MB
1GB
1KB
1MB
200MB
20MB
2MB
3MB
500MB
50MB
512KB
5MB"
  exit 2
fi
##
## Now start it
##
echo $1 "stream(s): http://speedtest.tele2.net/"$2
echo "starting NOW:"
for (( i=1; i<=$1; i++ ))
do
echo -ne "="
$command$url > /dev/null
done
echo -ne ">"
echo "DONE"
##
## Maybe you want to stop it
##
read -rsp $'Press enter to stop\n'
killall wget
echo "Bye!"
